/****************
Our first ServicWorker
*****************/
'use strict';

const version = 'v-01::';


const WORKERCONFIG = {

  "debug": true,

  "coreCacheName": version + 'core',
  "pagesCacheName": version + 'pages',
  "imagesCacheName": version + 'images',
  "assetsCacheName": version + 'assets',

  "coreCacheSize": 50,
  "pagesCacheSize": 20,
  "imagesCacheSize": 200,
  "assetsCacheSize": 35,


  "offlineUrl": "/offline.html",
  "offlineImg": "<svg viewBox=\"0 0 400 300\" xmlns=\"http://www.w3.org/2000/svg\">No Image</svg>",

  "coreCacheAssets": [
    "offline.html"
  ],

  "pathsBlocked": [
    "/admin"
  ]
};


// SW install and cache core assets
self.addEventListener('install', event => {
  event.waitUntil(
    // open a new cache and put all static files to it
    caches.open(WORKERCONFIG.coreCacheName).then((cache) => {
      return cache.addAll(WORKERCONFIG.coreCacheAssets);
    }).then(() => {
      return self.skipWaiting();
    })
  );
});

// SW activate and clear caches
self.addEventListener('activate', event => {
  event.waitUntil(
    clearCaches().then(() => {
      return self.clients.claim();
    })
  );
});

//fetch event for diffrent assets
self.addEventListener('fetch', event => {

  let request = event.request,
    acceptHeader = request.headers.get('Accept');

  (!!WORKERCONFIG.debug) ? console.log(acceptHeader): '';

  //If path blocked ?? return
  if (request.method == 'GET' && !isPathBlocked(request.url) && request.url.indexOf('http') !== -1) {

    // ** HTML Requests: network first ****************************************
    if (acceptHeader.indexOf('text/html') !== -1) {
      // Try network, then cache, then offline fallback

      (!!WORKERCONFIG.debug) ? console.log(WORKERCONFIG.pagesCacheName): '';

      event.respondWith(
        fetch(request)
        .then(response => {
          addToCache(WORKERCONFIG.pagesCacheName, request, response.clone());
          return response;
        })
        .catch(() => {
          return caches.match(request).then(response => {
            return response || caches.match(WORKERCONFIG.offlineUrl);
          });
        })
      );
    }

    // ** Non HTML Requests: caches first ****************************************
    if (acceptHeader.indexOf('text/html') == -1) {

      let cacheNameSwitch = (acceptHeader.indexOf('image') !== -1) ?
        WORKERCONFIG.imagesCacheName :
        WORKERCONFIG.assetsCacheName;

      (!!WORKERCONFIG.debug) ? console.log(cacheNameSwitch): '';

      event.respondWith(
        caches.match(request)
        .then(response => {
          // Try cache, then network, then offline fallback
          return response || fetch(request)
            .then(response => {
              addToCache(
                cacheNameSwitch, request, response.clone()
              );
              return response;
            })
        })
        .catch(() => {
          return new Response(WORKERCONFIG.offlineImg, {
            headers: {
              'Content-Type': 'image/svg+xml'
            }
          });
        })
      );
    }
  }
});

self.addEventListener("message", function(event) {
  var data = event.data;
  //Send this command whenever many files are downloaded (ex: a page load)
  if (data.command == "trimCache") {
    trimCache(WORKERCONFIG.coreCacheName, WORKERCONFIG.coreCacheSize);
    trimCache(WORKERCONFIG.pagesCacheName, WORKERCONFIG.pagesCacheSize);
    trimCache(WORKERCONFIG.imagesCacheName, WORKERCONFIG.imagesCacheSize);
    trimCache(WORKERCONFIG.assetsCacheName, WORKERCONFIG.assetsCacheSize);
  }
});

//Add to cache function
function addToCache(cacheName, request, response) {
  caches.open(cacheName)
    .then(cache => cache.put(request, response));
}

// cache clear function
function clearCaches() {
  return caches.keys().then(function(keys) {
    return Promise.all(keys.filter(function(key) {
      return key.indexOf(version) !== 0;
    }).map(function(key) {
      (!!WORKERCONFIG.debug) ? console.log('cache deleted: ', key): '';
      return caches.delete(key);
    }));
  })
}


// Trim specified cache to max size
var trimCache = function(cacheName, maxItems) {
  caches.open(cacheName)
    .then(function(cache) {
      cache.keys()
        .then(function(keys) {
          if (keys.length > maxItems) {
            cache.delete(keys[0])
              .then(trimCache(cacheName, maxItems));
          }
        });
    });
};


function trimSlash(str) {
  return str.replace(/(.)\/$/, '$1');
}

const pathsBlocked = new Set(WORKERCONFIG.pathsBlocked);

function isPathBlocked(pathname) {
  const path = trimSlash(pathname);

  (!!WORKERCONFIG.debug) ? console.log(path): '';

  for (let item of pathsBlocked) {
    if (path === item || path.startsWith(`${item}/`) || path.includes(item)) {
      return true;
    }
  }
  return false;
}

