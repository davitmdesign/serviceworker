#  Our first ServiceWorker

Debuggen und in Aktion sehen kann man ihn in der Chrome Entwickler Console unter "Application".

Was vom Worker ausgeliefert wird kann man im "Network" Tab sehen.

### ES6

Der Service Worker ist in ES& geschrieben, da alle Browser die Serviceworker unterstützen auch ES6 verstehen.


### Snippet von Google selber
```
https://googlechrome.github.io/samples/service-worker/custom-offline-page/
```

### Doku
```
https://developer.mozilla.org/de/docs/Web/API/Service_Worker_API/Using_Service_Workers
```

### Bereits implemntiert (Einfach mal rein schauen)
```
https://www.kinderzirkus-aron.de
```
